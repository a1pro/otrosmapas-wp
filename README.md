# Kobo Maps Plugin for WordPress

![Version](https://img.shields.io/badge/version-1.0.0--03012023-blue)
![WordPress](https://img.shields.io/badge/WordPress-5.2%2B-green)
![PHP](https://img.shields.io/badge/PHP-7.2%2B-blue)
![License](https://img.shields.io/badge/license-GPL3-red)

Kobo Mapas is a unique and innovative solution for integrating WordPress with KoboToolbox.

## Installation

1. Download the plugin ZIP file from [here](http://lalibre.net/kobo-maps).
2. Extract the ZIP file and upload the plugin folder to the `wp-content/plugins/` directory of your WordPress installation.
3. Activate the plugin through the WordPress admin panel.

## Usage

To use the Kobo Maps Plugin, you need to change the permalinks settings in WordPress. Follow these steps:

1. Go to the WordPress admin panel.
2. Navigate to **Settings** > **Permalinks**.
3. Select the **Post name** option.
4. Click **Save Changes**.

## Support

If you encounter any issues or have any questions, please visit our [support page](http://otrosmapas.org) for assistance.

## License

This plugin is licensed under the GPL3 License. See the [LICENSE](LICENSE) file for more details.
