function enqueue_custom_scripts() {
    wp_enqueue_script('custom-script', plugin_dir_url(__FILE__) . kobo-connector.js, array('jquery', 'leaflet'), '1.0', true);
}
add_action('wp_enqueue_scripts', 'enqueue_custom_scripts');