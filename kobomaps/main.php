<?php
/*
 * Plugin Name: Kobo Maps Plugin for WordPress
 * Plugin URI: http://lalibre.net/kobo-maps
 * Description: Kobo Mapas is a unique and innovative solution for integrating WordPress with KoboToolbox.
 * Version: 1.0.0-03012023
 * Requires at least: 5.2
 * Requires PHP: 7.2
 * Author: PNUD Ecuador / LaLibre.net
 * Author URI: http://otrosmapas.org
 * License: GPL3
 */
require __DIR__ . '/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\IOFactory;
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
include "helpers.php";

$upload_dir = wp_upload_dir();
$download_folder = $upload_dir['basedir'] . '/kobo_downloads/';
$download_url = $upload_dir['baseurl'] . "/" . "kobo_downloads/";


add_action('rest_api_init', function () {
	register_rest_route( 'map/v1', 'geticon',array(
				  'methods'  => 'post',
				  'callback' => 'get_icons'
		));
  });
  //geticon 
  function get_icons($request) {
    $postid = $request['postid'];
	$map_icon = $request['map_icon'];
	//echo $map_icon;die;
    $iconImage=get_post_meta( $postid, 'map_icons'.$map_icon )[0];
    echo str_replace('https:', '', $iconImage);
    // Ensure to handle the response appropriately based on your use case
    die;
}
add_action('rest_api_init', function () {
  register_rest_route( 'map/v1', 'formid/(?P<formid>\d+)',array(
                'methods'  => 'GET',
                'callback' => 'get_latest_posts_by_category'
      ));
});
//===== maps
function enqueue_leaflet_and_jquery() {
    // Enqueue Leaflet
    wp_enqueue_script('leaflet', '//unpkg.com/leaflet@1.9.4/dist/leaflet.js', array(), '1.9.4', true);
    wp_enqueue_script('leaflet-howler', '//unpkg.com/howler@2.2.4/dist/howler.js', array(), '2.2.4', true);
    wp_enqueue_style( 'lat__leaf_css', '//unpkg.com/leaflet@1.9.4/dist/leaflet.css', false, '1.9.4' );
    // Enqueue jQuery
    wp_enqueue_script('jquery');

    // Enqueue your custom script
}

// Hook into WordPress enqueue function
add_action('wp_enqueue_scripts', 'enqueue_leaflet_and_jquery');
function my_enqueue($hook) {

    wp_enqueue_script('leaflet', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js', array(), null, false);
}

add_action('admin_enqueue_scripts', 'my_enqueue');


function load_admin_style() {
    wp_enqueue_style( 'admin__leaf_css', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css', false, '1.0.0' );
}
add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function mytheme_add_init() {
    if ( is_admin() ) {
        $file_dir=get_bloginfo('template_directory');
        wp_enqueue_style("functions", $file_dir."/scripts/custom.css", false, "1.0", "all");
        wp_enqueue_script("rm_script", $file_dir."/scripts/custom.js", false, "1.0");
    }
}
add_action( 'wp_enqueue_scripts', 'mytheme_add_init' );

// Map management page


function leaflet_maps_shortcodes() {
    ob_start();
    ?>
    <div id="leaflet-map" style="height: 200px;"></div>
<!-- 
    <script>
       jQuery(document).ready(function() {
            // Fetch map data from the server using AJAX
            // Display the map with Leaflet
            var map = L.map('leaflet-map').setView([29.9680, 77.5552], 15);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
            L.marker([29.9680, 77.5552]).addTo(map)
                .bindPopup('test')
                .openPopup();
        });
    </script> -->
	<script>
    jQuery(document).ready(function () {
		
        // Fetch map data from the server using AJAX
        // Display the map with Leaflet
        var map = L.map('leaflet-map').setView([29.9680, 77.5552], 15);

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);

        // Define a custom icon
        var customIcon = L.icon({
			
            iconUrl: '<?php echo home_url("wp-content/plugins/kobomaps/assets/images/layers.png"); ?>',  // Replace with the path to your custom icon image
            iconSize: [32, 32],  // Size of the icon
            iconAnchor: [16, 32],  // Anchor point of the icon
            popupAnchor: [0, -32]  // Popup anchor point
        });

        // Use the custom icon for the marker
        L.marker([29.9680, 77.5552], { icon: customIcon }).addTo(map)
            .bindPopup('test')
            .openPopup();
    });
</script>
    <?php
    return ob_get_clean();
}

add_shortcode('leaflet', 'leaflet_maps_shortcodes');

function get_real_time_map_datas() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'leaflet_maps';
    $map = $wpdb->get_row("SELECT * FROM $table_name ORDER BY RAND() LIMIT 1", ARRAY_A);

    wp_send_json($map);
}

add_action('wp_ajax_get_real_time_map_data', 'get_real_time_map_datas');
add_action('wp_ajax_nopriv_get_real_time_map_data', 'get_real_time_map_datas');


//====mapsend
function get_latest_posts_by_category($request) {

    $koboUrl = 'https://kc.kobotoolbox.org';
    $apiEndpoint = $koboUrl . '/api/v1/data/'.$request['formid'];
    $uname = get_option("user_name");
    $pass = get_option("password");
    $response = wp_remote_get($apiEndpoint, array(
        'headers' => array(
            'Authorization' => 'Basic ' . base64_encode( $uname.":".$pass )
        ),
    ));
//echo "<pre>";print_r($response.response);die;
    if (is_wp_error($response)) {
        return $response;
    }	
	    echo $body = wp_remote_retrieve_body($response);
	    //$form_data = json_decode($body, true);
		    // echo  $form_data ;die;			
        //echo "fsdfsdfsd <pre>";print_r($form_data);die;
		//	$form_data;
die("");

    $args = array(
            'category' => $request['category_id']
    );

    $posts = get_posts($args);
    if (empty($posts)) {
    return new WP_Error( 'empty_category', 'There are no posts to display', array('status' => 404) );

    }

    $response = new WP_REST_Response($posts);
    $response->set_status(200);

    return $response;
}
function my_shortcode_function($atts)
{
	global $download_url;
	$atts = shortcode_atts(array(
		'id' => '1',
	), $atts, 'kobo-maps');

	wp_enqueue_script('leaflet-js', plugin_dir_url(__FILE__) . 'assets/leaflet.js', array('jquery'), '1.0', true);
	wp_enqueue_script('kobo-custom-js', plugin_dir_url(__FILE__) . 'assets/custom.js', array('jquery'), '1.0', true);

	$id = $atts["id"];

	$submission_number = get_post_meta($id, 'kobo-submission-number', true);

	if (!is_numeric($submission_number)) {
		$submission_number = 0;
	}


	$results = getSubmissionAsset($id, $submission_number);

	$custom_field_value = get_post_meta($id, 'kobo-select-fields', true);

	$lat = get_post_meta($id, 'kobo-lat-field', true);
	$long = get_post_meta($id, 'kobo-long-field', true);

	wp_localize_script('kobo-custom-js', 'kobo_map_data', array(
		'header' => $results[0],
		'body' => $results[1],
		'include' => $custom_field_value,
		'long' => $long,
		'lat'  => $lat,
		'center_lat' => $results[1][$lat][0],
		'center_long' => $results[1][$long][0],
		'file_upload_path' => $download_url
	));

	$data = "<div class='kobo-map-wrapper'>";

	$data .= "<div id='kobo-map'>";

	$data .= "</div>";

	$data .= "</div>";
	return $data;
}
add_shortcode('kobo-maps', 'my_shortcode_function');

// Grid column shortcode

// Add the custom columns to the book post type:
add_filter( 'manage_kobo_maps_posts_columns', 'set_custom_edit_kobo_maps_columns' );
function set_custom_edit_kobo_maps_columns($columns) {
    unset( $columns['meta_shortcode_map'] );
    $columns['meta_shortcode_map'] = __( 'Shortcode', 'your_text_domain' );
  //  $columns['publisher'] = __( 'Publisher', 'your_text_domain' );

    return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_kobo_maps_posts_custom_column' , 'custom_kobo_maps_column', 10, 2 );
function custom_kobo_maps_column( $column, $post_id ) {
    switch ( $column ) {

/*         case 'shortcode' :
            $terms = get_the_term_list( $post_id , 'shortcode' , '' , ',' , '' );
            if ( is_string( $terms ) )
                echo $terms;
            else
                _e( 'Unable to get shortcodes', 'your_text_domain' );
            break; */

       case 'meta_shortcode_map' :
            echo get_post_meta( $post_id , 'meta_shortcode_map' , true ); 
            break;

    }
}

function kobo_enqueue_assets()
{
	wp_enqueue_style('leaflet-css', plugin_dir_url(__FILE__) . 'assets/leaflet.css', array(), time(), 'all');
	wp_enqueue_style('kobo-custom-css', plugin_dir_url(__FILE__) . 'assets/custom.css', array(), '1.0', 'all');
}
add_action('wp_enqueue_scripts', 'kobo_enqueue_assets');


//enqueue only for kobo map posts on admin side
function enqueue_custom_script($hook)
{
	if ('post-new.php' === $hook || 'post.php' === $hook) {
		global $post;
		if ('kobo_maps' === $post->post_type) {
			wp_enqueue_style('admin-css', plugin_dir_url(__FILE__) .'assets/admin-style.css' , array(), '1.0', 'all');

			// Enqueue your script here
			wp_enqueue_script('select2-js', "https://cdn.jsdelivr.net/npm/select2/dist/js/select2.min.js", array('jquery'), '1.0', true);
			wp_enqueue_style('select2-css', "https://cdn.jsdelivr.net/npm/select2/dist/css/select2.min.css", array(), '1.0', 'all');
			wp_enqueue_script('kobo-admin-js', plugin_dir_url(__FILE__) . 'assets/admin.js', array('jquery'), '1.0', true);
		}
	}
}
add_action('admin_enqueue_scripts', 'enqueue_custom_script');




// Register Custom Post Type
function kobo_maps()
{

	$labels = array(
		'name'                  => _x('Kobo Maps', 'Post Type General Name', 'kobo'),
		'singular_name'         => _x('Kobo Map', 'Post Type Singular Name', 'kobo'),
		'menu_name'             => __('Kobo Maps', 'kobo'),
		'name_admin_bar'        => __('Kobo Maps', 'kobo'),
		'archives'              => __('Item Archives', 'kobo'),
		'attributes'            => __('Item Attributes', 'kobo'),
		'parent_item_colon'     => __('Parent Item:', 'kobo'),
		'all_items'             => __('All Items', 'kobo'),
		'add_new_item'          => __('Add New Item', 'kobo'),
		'add_new'               => __('Add New', 'kobo'),
		'new_item'              => __('New Item', 'kobo'),
		'edit_item'             => __('Edit Item', 'kobo'),
		'update_item'           => __('Update Item', 'kobo'),
		'view_item'             => __('View Item', 'kobo'),
		'view_items'            => __('View Items', 'kobo'),
		'search_items'          => __('Search Item', 'kobo'),
		'not_found'             => __('Not found', 'kobo'),
		'not_found_in_trash'    => __('Not found in Trash', 'kobo'),
		'featured_image'        => __('Featured Image', 'kobo'),
		'set_featured_image'    => __('Set featured image', 'kobo'),
		'remove_featured_image' => __('Remove featured image', 'kobo'),
		'use_featured_image'    => __('Use as featured image', 'kobo'),
		'insert_into_item'      => __('Insert into item', 'kobo'),
		'uploaded_to_this_item' => __('Uploaded to this item', 'kobo'),
		'items_list'            => __('Items list', 'kobo'),
		'items_list_navigation' => __('Items list navigation', 'kobo'),
		'filter_items_list'     => __('Filter items list', 'kobo'),
	);
	$args = array(
		'label'                 => __('Kobo Map', 'kobo'),
		'description'           => __('Post Type Description', 'kobo'),
		'labels'                => $labels,
		'supports'              => array('title'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type('kobo_maps', $args);
}
add_action('init', 'kobo_maps', 0);



function getSubmissionAsset($id, $submission_number)
{

	$file_path = get_transient('kobo_xlsx_download_' . $id);

	if ($file_path == null || !file_exists($file_path)) {

		//TODO: delete all existing xlsx files....
		// unlink($file_path);
		$plugin_path = plugin_dir_path(__FILE__);
		// $xlsx_files = glob($plugin_path . "*.xlsx");
		array_map('unlink', glob("$plugin_path*.xlsx"));

		$token = get_option("kobo_api_key");

		// $url = "https://kf.kobotoolbox.org/api/v2/assets/a9pbhwgyXVSynh9CohjEq6.json";
		$url = "https://kf.kobotoolbox.org/api/v2/assets.json";


		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_HTTPHEADER => array(
				'Authorization: Token ' . $token,
				'Cookie: django_language=en'
			),
		));

		$response = curl_exec($curl);

		// print_r($response);

		curl_close($curl);
		$data =  json_decode($response);


		$xlsxUrl = $data->results[$submission_number]->export_settings[0]->data_url_xlsx;

		// $last_data_results = end($data->results);
		// $xlsxUrl = $last_data_results->export_settings['0']->data_url_xlsx;


		$filename = uniqid(rand(), true) . '.xlsx';


		$file_path = $plugin_path . $filename;

		downloadFileWithAuthHeader($xlsxUrl, $token, $file_path);

		//kobo_xlsx_download_1 ...
		set_transient('kobo_xlsx_download_' . $id, $file_path, 10 * HOUR_IN_SECONDS); //clear every 1 hour
	}

	try {
		$spreadsheet = IOFactory::load($file_path);
		// $worksheet = $spreadsheet->getActiveSheet();
		$worksheet = $spreadsheet->getSheet(0);

		$row_first = true;
		$header = [];
		$body = [];

		foreach ($worksheet->getRowIterator() as $row) {

			$cellIterator = $row->getCellIterator();
			$cellIterator->setIterateOnlyExistingCells(FALSE);
			$row_count = 0;
			$cell_count = 0;
			foreach ($cellIterator as $cell) {
				if ($row_first) {
					//header cell
					$header[] = (string)$cell->getValue();
					//echo $cell->getValue() . "<br/>";
				} else {
					//regular cell
					if ($cell_count == 0 || $cell_count == 1) {
						$timestamp = (string)\PhpOffice\PhpSpreadsheet\Shared\Date::excelToTimestamp($cell->getValue());
						$body[$row_count][] = date("F j, Y, g:i a", $timestamp);
					} else {
						$body[$row_count][] = (string)$cell->getValue();
					}
				}

				$cell_count++;
				$row_count++;
				// echo $cell->getValue() . "\t";
			}
			// echo "\n";

			$row_first = false;
		}


		// echo "<pre>";
		//TODO: may also need header
		return array($header, $body);
		// print_r($body);
		// echo "</pre>";
	} catch (Exception $e) {
		return $e->getMessage();
	}
}


function kobomaps_custom_field_callback()
{
	add_meta_box(
		'kobomaps-custom-fields',
		'Map Settings',
		'kobo_custom_field_meta_box',
		'kobo_maps',
		'normal',
		'high'
	);
}
add_action('add_meta_boxes', 'kobomaps_custom_field_callback');

// leaf lat shortcode new
function getCenterLatLng($coordinates)
{
    $x = $y = $z = 0;
    $n = count($coordinates);
    foreach ($coordinates as $point)
    {        
        $lt = $point['lat'] * pi() / 180;
        $lg = $point['long'] * pi() / 180;
        $x += cos($lt) * cos($lg);
        $y += cos($lt) * sin($lg);
        $z += sin($lt);
    }
    $x /= $n;
    $y /= $n;

    return [atan2(($z / $n), sqrt($x * $x + $y * $y)) * 180 / pi(), atan2($y, $x) * 180 / pi()];
}
function leaflat_map($atts) {
    $default = array(
        'id' => '#',
    );
    $a = shortcode_atts($default, $atts);
	   // ob_start();
	      $postId=$a['id'];
	      $myvals = get_post_meta($a['id']);
		  $key_1_values = get_post_meta($a['id'], 'meta_location_arr' );
          $checked_field_values =get_post_meta($a['id'], 'checked_field' );
          $wp_attached_file =get_post_meta($a['id'], 'map_icons1' );
		  //print_r( );die;
		  $post_id = get_post_meta( $wp_attached_file[0] ,'map_icons1  ');
          $icon =$wp_attached_file[0];
		 
		  $key_meta_form_id =get_post_meta($a['id'], 'meta_form_id' );
          $meta_form_id =$key_meta_form_id[0];
          $field_values=$checked_field_values[0];
          $arr_field_values=explode(",",$field_values);
          //$attch=array("_attachments");
          //$arr_field_values = array_merge($attch, $arr_field_values); 

		  $image_map = $icon ;
		 
          //echo "Key: ".$checked_field_values[0];
		  //$key_1_values = get_post_meta($a['id'], 'meta_location_arr' );
		  // Decode the JSON string into a PHP array
		 $arrayFromJson = json_decode($key_1_values[0], true);
         $mapCenter=getCenterLatLng($arrayFromJson);
		// $map= '<div id="leaflet-map-loader"></div><button id="fullscreen-button">Toggle Fullscreen</button><div id="fullscreen-container"><h1>Your Content Goes Here</h1><div id="leaflet-map" style="height: calc(100vh);max-width: 97%;width: 100%;position: absolute;outline-style: none;top: 0%;left: 2%;"></div></div>';
            $map= '<div id="leaflet-map-loader"></div><div id="fullscreen-container"><button id="fullscreen-button"style="padding: 4px 6px;background-color: white;margin-left: 11px;border: 1px solid grey;"><span aria-hidden="true">🔍</span></button><div id="leaflet-map" style="height: 100vh;"></div></div>';				  $map .=' <script>
                    const fullscreenButton = document.getElementById("fullscreen-button");
                              const fullscreenContainer = document.getElementById("fullscreen-container");

                              fullscreenButton.addEventListener("click", () => {
                                if (document.fullscreenElement) {
                                  // If in fullscreen, exit fullscreen
                                  document.exitFullscreen();
                                } else {
                                  // If not in fullscreen, request fullscreen
                                  fullscreenContainer.requestFullscreen().catch(err => {
                                    console.error("Fullscreen request failed:", err);
                                  });
                                }
                              });
								   jQuery(document).ready(function() {
                                    jQuery("#leaflet-map-loader").append("<img id=map-loader src='.site_url().'/wp-content/plugins/kobomaps/assets/images/map-loader.gif>");
								// Fetch map data from the server using AJAX
								   var map = L.map("leaflet-map").setView(['.$mapCenter[0].', '.$mapCenter[1].'],9); // Adjust the initial view and zoom level
                                   var resp = [];
									L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(map);
                                   jQuery.ajax({
									url: "'.site_url().'/wp-json/map/v1/formid/'.$meta_form_id.'",
										success: function (response) {
                                           // console.log(response,"Form Data Response")                                         
												resp.push.apply(resp, response);									
									},
									error: function(jqXHR, textStatus, errorThrown) {
										console.log(textStatus, errorThrown);
									}
									});
									// var finalData = JSON.parse(result);
									//	var JsonObject = JSON.parse(resp);
									// Function to get values for the specified keys
								function getValuesForKeys(obj, keys) {
									//  return keys.map(key =>  {key: obj[key]}});
                                         console.log(obj,"objjjj")
										return keys.map(key => ({ [key]: obj[key] }));

                                }function getValuesImgSrc(downloadImgUrl) {
                                     document.getElementById("mapImg").src = "ssss";
                                }	
								

                              //  window.resp = resp;
								      setTimeout(function() {
                                          jQuery("#map-loader").remove();
										  
                                             jQuery("#leaflet-map").attr("style","visibility:visible;height: 100vh;")
                                        //   jQuery("#leaflet-map").attr("style","visibility:visible;height: calc(100vh);max-width: 97%;width: 100%;position: absolute;outline-style: none;top: 0%;left: 2%;")
													L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(map);
													// Define an array of marker positions
													var markerPositions = '.json_encode($arrayFromJson, true).' ;
													let displayFieldArr = '.json_encode($arr_field_values, true).';
															// Loop through the array and add markers to the map
													var number = 1;
													
													for(let i=0; i <markerPositions.length;i++) {
														var lat=markerPositions[i].lat;
														var long=markerPositions[i].long;
														
                                                        console.log(markerPositions[i],"markerPositions");
														var arr12 = resp.find(d => d._id == markerPositions[i]._id);
                                                        console.log(resp,"resp");
                                                        console.log(arr12,"arr12");
                                                        //console.log(Object.keys(arr12).length,"arr12.length");
														
                                                        if(Object.keys(arr12).length>0){
															console.log(arr12,"log");
															let htmlTag="";
															let htmlTagText="";
															let htmlTagImage="";
															let htmlTagaudio="";
															const valuesForKeys = getValuesForKeys(arr12, displayFieldArr);
															valuesForKeys.map(obj => {
															const keyValueObject = {};
															Object.keys(obj).forEach(key => {
																

															if(key){
                                                             if(key !=="_attachments" ){
																
                                                                    keyValueObject[key] = obj[key];
															        htmlTag += "<tr><td>"+key.replace(/\_/g," ")+"</td><td><div id=1><p>"+obj[key]+"</p></div></td></tr>";                                                                 
                                                                   
                                                                } else {
                                                                    var json=obj[key];
                                                                 var imgPath=json.filter(x=> x.mimetype == "image/jpeg");
                                                                if(json.filter(x=> x.mimetype == "audio/ogg").length>0){
                                                                  var audioPath=json.filter(x=> x.mimetype == "audio/ogg");  
                                                                 } else {
                                                                  var audioPath=json.filter(x=> x.mimetype == "audio/mp3");   
                                                                 }
                                                                 console.log("audioPath---",audioPath);
                                                                 var videoPath=json.filter(x=> x.mimetype == "video/mp4");
																
                                                                 const download_large_url = imgPath[0].download_large_url;
																 console.log("kpl" + download_large_url);
                                                                 const video_download_large_url = videoPath[0].download_large_url;
																 var vediourl = video_download_large_url;
																if (audioPath.length < 1) {
															htmlTag += `<tr><p>
															<td colspan=2><video width="320" height="240" controls>
															<source src="${vediourl}" type="video/mp4">														
														  </video>  
																		</p></tr>`;
                                                                   htmlTag += "<tr><td colspan=2><div id=2><p><img"+" "+"width=100"+" "+"height=100"+" "+"src="+download_large_url.replace(/ +/g, "")+"></p></div></tr></td>";                                                                 
                                                                } else {
																	const audio_large_url=audioPath[0].download_large_url;
																	console.log(key+":"+audio_large_url);  
																	var audioUrl = audio_large_url;
																htmlTag += `<audio class=custom-audio controls style=>
																   <source src="${audioUrl}" type="audio/mpeg">
																Your browser does not support the audio element.
															    </audio>`;	
						                                     htmlTag += "<tr><div id=3><p></tr><tr><td colspan=2><img"+" "+"src="+download_large_url.replace(/ +/g, "")+"></p></div></td></tr>";                                                                 

															            }
                                                                // window.downloadImgUrlw = download_large_url;
                                                                // window.mapImgw = "mapImg"+i;
                                                                //  setTimeout(function(){
                                                                // var test2  = downloadImgUrlw || []; console.log(downloadImgUrlw); 
                                                                // var test4  = mapImgw || []; console.log(mapImgw); 
                                                                // document.getElementById(mapImgw).src = downloadImgUrlw;
                                                               // },3000); 
                                                                }

																}
																});
															});
														
															var popup = L.popup({className: "if-you-need-a-class"}).setContent("<div class=if-you-need-div style=display:flex;flex-direction:column-reverse;><table><tr><th>Field type</th><th>View</th>"+htmlTag+"</tr></table></div>");
															var bb="map_icons"+number;
															var mapIconurl=jQuery.ajax({
																url: "'.site_url().'/wp-json/map/v1/geticon",
																data: { postid:' .$postId.' ,
																	    map_icon :  number 
																		 
																},
                                                                dataType:"html",global: false, async:false,
																method:"POST",
																success: function (response) {
                                                                    return response;
																},
																error: function(jqXHR, textStatus, errorThrown) {
																	console.log(textStatus, errorThrown);
																}
															}).responseText;;


															console.log(mapIconurl," image url");
                                                            if(mapIconurl){
                                                                var customIcon = L.icon({
																iconUrl: mapIconurl,
																iconSize: [32, 32],  // Size of the icon
																iconSize: [32, 32],  // Size of the icon
																iconAnchor: [16, 32],  // Anchor point of the icon
																popupAnchor: [0, -32]  // Popup anchor point
															});
                                                            L.marker([lat,long], { icon: customIcon }).addTo(map)
																		.bindPopup(popup)

																	.openPopup();
                                                            } else {
															var customIcon = L.icon({
																iconSize: [32, 32],  // Size of the icon
																iconSize: [32, 32],  // Size of the icon
																iconAnchor: [16, 32],  // Anchor point of the icon
																popupAnchor: [0, -32]  // Popup anchor point
															});
                                                            L.marker([lat,long]).addTo(map)
															.bindPopup(popup)

																	.openPopup();                                                                    
                                                                       
                                                            }

																
																	number++;
                                                                       };};
    
                                                               jQuery(".playAudio").on("click", function() {
                                                                      // Your click event handling code here
                                                                        var customValue = jQuery(this).attr("audiourl");
                                                                        var audio = new Howl({
                                                                        src: [customValue],
                                                                        format: ["opus"],
                                                                        html5: true
                                                                    });
                                                                    //audio.play();
                                                                    //console.log(audio.playing()); 
                                                                    var myMusicID = audio.play();                                                                    
                                                                    var paused = true;
                                                                    var saveSeek;
                                                                     if(jQuery(this).text()=="Pause"){                                                                         
                                                                            audio.pause();
                                                                            saveSeek = audio.seek(myMusicID);
                                                                            jQuery(this).text("Play Audio");
                                                                     } else {                                                                        
                                                                             audio.play(myMusicID);
                                                                             audio.seek(saveSeek, myMusicID);
                                                                             jQuery(this).text("Pause");
                                                                     }
                                                                    
                                                                      //audio.play();
                                                                    });
                                                           

                                                                                                                        
		}, 3500,resp);

				                                             				
											});
							</script>';				

			
	      return $map;die;exit;
	   $map= '<div id="leaflet-map" style="height: 200px;"></div>';
       $lat_lon=[];
	   foreach($myvals as $key=>$val)
		 { 
		    if (str_contains($key, 'meta_')) { 
			 if (str_contains($key, 'meta_name')) {
				 $name=$val[0];
			 }
			 if (str_contains($key, 'meta_latitude')) {
				 $latitude=$val[0];
			 }
			 if (str_contains($key, 'meta_longitude')) {
				 $longitude=$val[0];
			 }
			 if($name &&  $latitude &&  $longitude){ 
				$map .="<script>
				jQuery(document).ready(function() {
					 // Fetch map data from the server using AJAX
					 // Display the map with Leaflet
					 var map = L.map('leaflet-map').setView([".$lat_lon."], 15);
					 L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png').addTo(map);
					 L.marker([".$lat_lon."]).addTo(map)
						 .bindPopup(".$name.")
						 .openPopup();
				 });
			 </script>";
			 }

            }
		 
		 }

		//	return ob_get_clean();
	return $map;
  //  return 'Follow us on '.$a['id'];
}
add_shortcode('leafletmap', 'leaflat_map');

include( plugin_dir_path( __FILE__ ) . '/map_icon.php');

// get forrms from kobo
function kobo_get_forms()
{
    $koboUrl = get_option("get_url");
    $apiEndpoint = $koboUrl . '/api/v1/forms';
    $uname = get_option("user_name");
    $pass = get_option("password");
    $response = wp_remote_get($apiEndpoint, array(
        'headers' => array(
            'Authorization' => 'Basic ' . base64_encode( $uname.':'.$pass )
        ),
    ));
//echo "<pre>";print_r($response.response);die;
    if (is_wp_error($response)) {
        return $response;
    }	
	    $body = wp_remote_retrieve_body($response);
	    $form_data = json_decode($body, true);
		    // echo  $form_data ;die;			
       // echo "fsdfsdfsd <pre>";print_r($form_data);die;
			return $form_data;
}
function kobo_custom_field_meta_box($post)
{
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
    $formArr=kobo_get_forms();
		//print_r($formArr);die;

	/*$submission_number = get_post_meta($post->ID, 'kobo-submission-number', true);

	if (!is_numeric($submission_number)) {
		$submission_number = 0;
	}

	$xlsx_data = getSubmissionAsset($post->ID, $submission_number);
	$header = $xlsx_data[0];
	$body = $xlsx_data[1];

	$custom_field_value = get_post_meta($post->ID, 'kobo-select-fields', true);

	if (!is_array($custom_field_value)) {
		$custom_field_value = [];
	}*/

	//echo "Shortcode: <code>[kobo-maps id='{$post->ID}']</code><br/><br/>";

	/* foreach ($body as $key => $value) {

		if (in_array($key, $custom_field_value)) {
			foreach ($value as $key2 => $value2) {
				if (filter_var($value2, FILTER_VALIDATE_URL) !== FALSE) {
					//echo $value2;
					//echo "------------------------<br/>";
					download_and_save_file_with_curl($value2);
				}
			}
		}
	}*/
	echo "
		<div id='leaflet-map' style='height: 400px; display:none;' ></div>
        <div id='checkbox-values-saved'></div>
	
		<style>
		.wp-core-ui select[multiple] {
			overflow: scroll;
			height: 156px;
			padding-right: 8px;
			background: #fff;
		}
		.wp-core-ui select:focus {
			width: 100%;
			border-color: #2271b1;
			color: #0a4b78;

		}
		.wp-core-ui select:hover {
			color: black;
		}
		select#rightValues option {
			background: white!Important;
			padding: 2px 0;
		}
		select#leftValues option {
			background: white!Important;
			padding: 2px 0;
		}
        div#formInputsContainer .row div {
				border-bottom: 1px solid #726464;
				padding: 15px;
				width: 100%;
				float: left;
            }.inner-content label,div#formCheckboxContainer label{font-weight:bold;}.inner-content input{padding:5px;}
			.inner-content {
                border: none !important;
                width: 30% !important;
                float: left !important;
			}.checkboxes-container {
                width: 33.3%;
                float: left;
            }div#formCheckboxContainer {
                display: block;
                width: 100%;
                float: left;
            }

    
	.inner-content label {
	   display: none;
   }
   tr.inner-row_1 .inner-content input{
    background: white;
    padding: 6px 5px;
    margin-top: 11px;
}
section.container div:nth-child(1):before {
	padding: 8px 0px;
    font-weight: bold;
	font-size:15px;
    content: 'selectable';
    display: block;
    top: 0;
}
section.container div:nth-child(3):before {
	text-align: left;
    content: 'selected';
	padding: 8px 0px;
    font-weight: bold;
	font-size:15px;
    display: block;
    top: 0;
}
   .inner-content br {
    display: none;
}
   td{
	padding:5px;
	border:1px solid lightgray;
   }
   tr.inner-row_1 label {
	   display: block;
	   font-size:15px;
   }
   div#formInputsContainer .row div{
	display:contents;
	padding:6px 10px!important;
   }
  table {
    background: #fff;
    box-shadow: 0px 2px 1px -1px rgba(0,0,0,0.2), 0px 1px 1px 0px rgba(0,0,0,0.14), 0px 1px 3px 0px rgba(0,0,0,0.12);
    margin-top: 20px;
    padding: 20;
	border:1px solid;
	border-radius:5px;
}
tr.inner-row_1 {
    background: rgba(0, 0, 0, 0.04);
}
.inner-content input{
	border:none;
	background:transparent;
}
.inner-content input:focus-visible{
	outline:none;
	border:none;
}
div#formCheckboxContainer{
	justify-content:start!important;
	gap:00px;
}
.checkboxes-container{
	flex-direction: row-reverse;
	display:flex;
	align-items:center;
	gap:5px;
}
input[type=checkbox]{
	margin:0!important;
}
div#checkbox-values-saved{
	word-wrap: break-word;
    text-wrap: balance;
}

SELECT, INPUT[type=text] {
    width: 160px;
    box-sizing: border-box;
}
SECTION {
    padding: 8px;
    background-color: #f0f0f0;
    overflow: auto;
}
SECTION > DIV {
    float: left;
    padding: 4px;
}
SECTION > DIV + DIV {
   
    text-align: center;
}
section.container {
    display: flex;
    align-items: center;
    justify-content: start;
}
.modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content/Box */
.modal-content {
    background-color: #fefefe;
    margin: 15% auto; /* 15% from the top and centered */
    padding: 20px;
    border: 1px solid #888;
    width: 80%; /* Could be more or less, depending on screen size */
}

/* Close Button */
.close {
    color: #aaa;
    float: right;
    font-size: 28px;
    font-weight: bold;
}

.close:hover,
.close:focus {
    color: black;
    text-decoration: none;
    cursor: pointer;
}
		</style>
	";	
	echo "<div style='display:none;'><label for='shortcodeid'>Shortcode</label><textarea id='shortcodeid'></textarea></div>";
//	echo "<div><label for='kobo-submission-number'>Choose Submission Number</label></div>";
	  $myvals = get_post_meta($post->ID);
	 foreach($myvals as $key=>$val)
		 {
            // if($key!=='_edit_lock' && $key!=='_edit_last' && $key!=='meta_kobo_submission_forms' && $key=='checked_field'&& $key!=='shortcode_map'){ ?>
   
          <?php   
             if($key!=='_edit_lock' && $key!=='_edit_last' && $key!=='meta_kobo_submission_forms' && $key!=='shortcode_map'){ ?>
			  <script>
			 jQuery(document).ready(function () {
				  jQuery("input[name=meta_location_arr]").remove();
				   //jQuery('#formInputsContainer .row .inner-row input').remove();
				 //  jQuery('#formInputsContainer .row').append('<div class=inner-row></div>');
				  // jQuery('#formInputsContainer .row').append('<div class=inner-content><input name="<?php echo  $key; ?>" value=<?php echo $val[0]; ?> /></div><br />');
			 });
			 </script>
		 <?php }
		 
		 }
	$key_1_values = get_post_meta( $post->ID, 'meta_kobo_submission_forms' );
	$checked_field_values = get_post_meta( $post->ID, 'checked_field' );
	$checked_field_valuess = get_post_meta( $post->ID, 'checked_field' );
	
	$cleaned_values = array();

	// Loop through each value in $checked_field_valuess
	foreach ($checked_field_valuess as $value) {
		// Remove any commas from the current value and add it to the cleaned_values array
		$values_array = explode(',',  $value);
		
	}
	

	$meta_form_id = get_post_meta( $post->ID, 'meta_form_id' );
    if($checked_field_values){
		echo '<script>
		jQuery(document).ready(function () {
			jQuery("#checkbox-values-saved").append("<b>'.$checked_field_values['1'].'</b>");
			
			'; // End the echo statement before entering PHP mode
			
			foreach ($values_array as $values) {
				// Outputting the line to append an option to #rightValues within the loop
				echo '$("#rightValues").append($("<option>").text("' .$values . '"));';
			}
			
			echo '}); // End of document ready function
	</script>';
         }

	
	//echo $key_1_values[0];
	
	$screen = get_current_screen();
	echo "<div><select id='kobo-submission-forms' name='meta_kobo_submission_forms'><option value=''>Select Project</option>";
	foreach($formArr as $form){ ?>
	<option value="<?php echo $form['formid']; ?>" ><?php echo $form['title']; ?></option>
	<?php }	
	
	
	
	echo "</select><div id='formCheckboxContainer'></div><div id='formInputsContainer'><div class=row><table></table></div></div></div><small>After changing the submission number, save the page before seeing the new data reflected in the fields below.</small><br/><br/></div>";
	
	echo "</select><div id='formCheckboxContainer'></div><div id='icons'><div class=row></div></div></div>";
	
	
	// if ( $screen->parent_base == 'edit' ) {
	  echo "<input name='meta_shortcode_map' type='text' value='[leafletmap id=".$post->ID."]' />";
      if($meta_form_id[0]){
	  echo "<input id='meta_for_id' name='meta_form_id' type='hidden' value='".$meta_form_id[0]."' />";
      } else {
	  echo "<input id='meta_for_id' name='meta_form_id' type='hidden' />";
      }
	//}
// admin fields generation
$screen = get_current_screen();
if($screen->post_type=='kobo_maps'){
    $key_1_values = get_post_meta( $_GET['post'], 'meta_kobo_submission_forms');
}
     if($key_1_values[0]){
         $selectedProject=$key_1_values[0];
     } else {
         $selectedProject="";
     }
	 echo '<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>';
	 echo '<script>
	 var selectElement = document.getElementById("kobo-submission-forms");

	 // Set the value you want to select
	 var valueToSelect = "' . $selectedProject . '";

	 // Set the value of the select element to the desired value
	 selectElement.value = valueToSelect;
			 // Your code to execute when the option with value 8 is selected
			 console.log("Option with value 8 is selected!");

	 </script>';
	echo '<script>
	 jQuery(document).ready(function () {
		console.log("here");

	    var inputCheckboxes;
		   jQuery("#kobo-submission-forms").change(function(){
                            // Get the select element
						
			
			   var html="";
			   let arr=[];
               let LatLon=[];
			jQuery("#leaflet-map").attr("style","height: 400px; display:block;");
			jQuery("#meta_for_id").val(jQuery(this).val());
			jQuery("#formInputsContainer .row > label").remove();
			jQuery("#icons .row > label").remove();
			jQuery("#formInputsContainer #location_arr").remove();
			jQuery("#formCheckboxContainer .checkboxes-container").remove();
			
						 
			jQuery.ajax({
				url: "'.site_url().'/wp-json/map/v1/formid/"+jQuery(this).val(),
				success: function (data) {
					 var inc=1;
                     var iterator=1;
				 jQuery(data).each(function (index, item ) {
					
                     if(iterator < 2){
                         jQuery.each(item, function(k1, v1) {
                console.log(k1+"=> "+v1,"items-iteration") 
				if(k1!=="formhub/uuid" && k1!=="meta/instanceID"   && k1!=="_submitted_by"  && k1!=="_uuid" && k1!=="_xform_id_string" && k1!=="_validation_status" && k1!=="_tags" && k1!=="_submission_time" && k1!=="_status" && k1!=="__version__" && k1!=="__version__" && k1!=="_id"){
				console.log(k1+"=> "+v1,"items-iteration") 
				var inputCheckboxes = jQuery("<div class=checkboxes-container><label>" + k1.replace(/\_/g, " ") + "</label> <input name=checked_field[] type=checkbox value=" + k1 + " ><br /></div>");
				
				
				jQuery("#formCheckboxContainer").append(inputCheckboxes);

					}
					
				})
				//jQuery("#formCheckboxContainer").append("<div class=inner-row_"+inc+"></div>");
                      
                     }
                     iterator++;                     
                 });
				 jQuery(data).each(function (index, item ) {
                     					 var a=1;
                 if(inc<5){                        

					 var dataObject = {
						            name:item.name,
									lat: item.latitude,
									long:item.longitude
								};
				     //jQuery("#formInputsContainer .row > .inner-row").remove();
					 jQuery("#formInputsContainer .row table").append("<tr class=inner-row_"+inc+"></tr>");
                	jQuery("#icons .row").append("<div class=inner-row_"+inc+"></div>");
					 console.log(item,"items")
					 //jQuery("#submitions").append("<option value=item>"+item.name+"</option>")
					 var input_icon = jQuery("<input type=hidden id=hiddenInput" + inc + " name=icon value= ><br />");
					 var input1 = jQuery("<td><div class=inner-content><label>Icon</label><br /><input class=pro_img name=icons" + inc + "  id=fileInput" + inc + " type=file value=></div></td>");
					

					 jQuery("#formInputsContainer .row .inner-row_" + inc).append(input_icon).append(input1);
					
			 
					 jQuery("#fileInput" + inc).on("change", function () {
						 var fileInput =jQuery(this)[0];
						 var fileName = fileInput.files[0].name;
						 jQuery("#hiddenInput" + inc).val(fileName); // Update the hidden input with the file name
						 
					 });

					 
			
					// Close popup when close button is clicked
					$("#closeBtn").on("click", function() {
						$("#popupForm").hide();
					});
			
					// Submit form data
					$("#myForm").on("submit", function(event) {
						event.preventDefault(); // Prevent default form submission
			
						// Retrieve form data
						var formData = $(this).serializeArray();
						console.log(formData); // Just for demonstration, you can handle the form data as needed
			
						// Close the popup after form submission
						$("#popupForm").hide(); // Update the hidden input with the file name
						
					});

					
					jQuery.each(item, function(k, v) {
						//display the key and value pair
						if(k=="latitude" ){
							let lat11=v;
						} 
						if(k=="longitude"){
                         	let long=v;
						}	
                    if(k!=="formhub/uuid" && k!=="meta/instanceID"   && k!=="_submitted_by"  && k!=="_uuid" && k!=="_xform_id_string" && k!=="_validation_status" && k!=="_tags" && k!=="_submission_time" && k!=="_status" && k!=="__version__" && k!=="__version__"){
						if(k=="latitude" ||k=="longitude" ) {	
							 
							    
						  var input = jQuery("<input type=hidden id="+k+" name=meta_"+k+a+" value="+v+" /><br />");
						  jQuery("#formInputsContainer .row .inner-row_"+inc+"").append(input);
	
						 } else {
						  var v=JSON.stringify(v) ;
                            var input1 = jQuery("<td><div class=inner-content><label for="+k+">"+k+"</label><br /><input id="+k+" name=meta_"+k+a+" value="+v+" readOnly></div></td>");
						  jQuery("#formInputsContainer .row .inner-row_"+inc+"").append(input1); 
						  
						 }
						
					}	
						//console.log(k + " is " + v);					
					  						a++;
					
					});
					
                      LatLon.push(dataObject);
					// Define an array of marker positions
					// var data=[item.latitude,item.longitude];
                    //console.log(item._geolocation[0],"geop")
                    if(item._geolocation[0]!==null && item._geolocation[1]!==null){
					   var data={"_id":item._id,"lat":item._geolocation[0],"long":item._geolocation[1]};
					 arr.push(data);
                    }
			
					 //jQuery("#shortcodeid").val(data);

					  // console.log(index +JSON.stringify(item) +"hererere")
					   // each iteration
					  // var result = item.Result;
					  // var isAdmin = item.isAdmin;
					  // var CustomerCode = item.CustomerCode;
					  // var CompanyCode = item.CompanyCode;
                 }
					 inc++;
				   });
                     
                        var inputArr = jQuery("<input type=text id=location_arr name=meta_location_arr value="+JSON.stringify(arr)+" /><br />");
                        jQuery("#formInputsContainer").append(inputArr);
                        var map = L.map("leaflet-map").setView([arr[0].lat, arr[0].long], 7); // Adjust the initial view and zoom level

                        L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png").addTo(map);		

					  for (var i = 0; i < arr.length; i++) {
												var lat = parseFloat(arr[i].lat);
												var lng = parseFloat(arr[i].long);	
																			
											    var popup = L.popup({className: "'.'if-you-need-a-class'.'"})
									            .setContent("<div class=if-you-need-div><h3>"+arr[i].name+"</h3><p><b><br /><b></b><br></p></div>");
                                                //console.log(lat,lng)
												L.marker([lat, lng]).addTo(map)
												//.bindPopup("")
												.openPopup();
											              }
															}
														});
												 	});

														});
														
														</script>';
															echo "
															<script>
															setTimeout(function() {
																	// Trigger the select event on page load
																	jQuery('#kobo-submission-forms').trigger('change'); 
												}, 500);            
															</script>
												
			                                                 ";

															 
															 echo '
															 
														<section class="container">
															<div>
																<select id="leftValues" size="5" multiple></select>
															</div>
															<div>
																<input type="button" id="btnLeft" value="&lt;&lt;" />
																<input type="button" id="btnRight" value="&gt;&gt;" />
																
															</div>
															<div>
																<select id="rightValues" size="4" multiple>
																
																
																</select>
																
															</div>
														

														</section>';


echo '
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready(function() {
    $("#btnLeft").on("click", function() {
        var selectedItem = $("#rightValues option:selected");
        $("#leftValues").append(selectedItem);
    });

    $("#btnRight").on("click", function() {
        var selectedItem = $("#leftValues option:selected");
        $("#rightValues").append(selectedItem);
    });

    $("#rightValues").on("change", function() {
        var selectedItem = $("#rightValues option:selected");
        $("#txtRight").val(selectedItem.text());
    });
});
</script>';
echo '
<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}
</script>';
	die;exit;
	

/*
          //  $("#movie-data").append(html);

	echo "<div><label for='kobo-submission-number'>Choose Submission Number</label></div>";
	echo "<div><input type='number' id='kobo-submission-number' name='kobo-submission-number' value='{$submission_number}'></div><small>After changing the submission number, save the page before seeing the new data reflected in the fields below.</small><br/><br/>";


	echo "<div><label for='kobo-select-fields'>Choose Fields to show:</label></div>";
	echo "<div><select id='kobo-select-fields' name='kobo-select-fields[]' multiple>";
	foreach ($header as $key => $value) {
		$selected_string = in_array($key, $custom_field_value) ? "selected" : "";
		echo "<option value='{$key}' {$selected_string}>{$value}</option>";
	}
	echo "</select></div>";


	$kob_lang_field = get_post_meta($post->ID, 'kobo-long-field', true);

	echo "<div style='margin-top: 20px;'><label for='kobo-long-field'>Choose the longitude field:</label></div>";
	echo "<div><select id='kobo-long-field' name='kobo-long-field' >";
	foreach ($header as $key => $value) {
		$selected_string = $kob_lang_field == $key ? "selected=selected" : "";
		echo "<option value='{$key}' {$selected_string}>{$value}</option>";
	}
	echo "</select></div>";

	$kob_lat_field = get_post_meta($post->ID, 'kobo-lat-field', true);


	echo "<div style='margin-top: 20px;'><label for='kobo-lat-field'>Choose the latitude field:</label></div>";
	echo "<div><select id='kobo-lat-field' name='kobo-lat-field' >";
	foreach ($header as $key => $value) {
		$selected_string = $kob_lat_field == $key ? "selected=selected" : "";
		echo "<option value='{$key}' {$selected_string}>{$value}</option>";
	}
	echo "</select></div>";*/
} //eof


function kobo_save_custom_fields($post_id)
{
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

	if ('kobo_maps' == get_post_type($post_id)) {


		if (isset($_POST['kobo-select-fields'])) {
			$custom_field_value = $_POST['kobo-select-fields'];
			//print_r($custom_field_value);
			update_post_meta($post_id, 'kobo-select-fields', $custom_field_value);
		}

		if (isset($_POST['kobo-long-field'])) {
			$long_field = $_POST['kobo-long-field'];
			//print_r($custom_field_value);
			update_post_meta($post_id, 'kobo-long-field', $long_field);
		}

		if (isset($_POST['kobo-lat-field'])) {
			$lat_field = $_POST['kobo-lat-field'];
			//print_r($custom_field_value);
			update_post_meta($post_id, 'kobo-lat-field', $lat_field);
		}

		if (isset($_POST['kobo-submission-number'])) {
			$sub_num = $_POST['kobo-submission-number'];
			update_post_meta($post_id, 'kobo-submission-number', $sub_num);
		}
	}
}
add_action('save_post', 'kobo_save_custom_fields');
add_action('save_post','save_post_callback');
// Save custom fields new
function save_post_callback($post_id){
    global $post;
    $a=1;
    foreach($_FILES as $file){
        if($_FILES['icons'.$a]['name']){
        $uploaddir = $_SERVER['DOCUMENT_ROOT']."wp-content/uploads/";
        $ext = pathinfo($_FILES['icons'.$a]['name'], PATHINFO_EXTENSION);
		$imageName = $a . time() . "." . $ext;
		$uploadfile = $uploaddir . basename($a . time() . "." . $ext);
		$uploadfileUrl = site_url() . "/wp-content/uploads/" . $imageName;

            if (move_uploaded_file($_FILES['icons'.$a]['tmp_name'], $uploadfile)) {
                			           $map_icons_values = get_post_meta( $post_id, "map_icons".$a );
											if( $map_icons_values[0]) {
													$current_custom_postmeta = get_post_meta($post_id, "map_icons".$a, true);
                                                    if($current_custom_postmeta)
													{
														delete_post_meta($post_id, "map_icons".$a);
													}
												 add_post_meta($post_id, "map_icons".$a, $uploadfileUrl, true); // unique

											} else {
                                             //echo $post_id."=>".$checkedField;

											  add_post_meta($post_id, "map_icons".$a, $uploadfileUrl, true); // unique
											}
            } else {
            }
            $a++;
    }
    }
    
    if ($post->post_type == 'kobo_maps'){
        //print_r($_POST['checked_field']);
               if($_POST['checked_field']){
                    $checkedField = implode(',', $_POST['checked_field']);
											$key_1_values = get_post_meta( $post_id, "checked_field" );
											if( $key_1_values[0]) {
													$current_custom_postmeta = get_post_meta($post_id, "checked_field", true);
                                                    if($current_custom_postmeta)
													{
														delete_post_meta($post_id, "checked_field");
													}
												 add_post_meta($post_id, "checked_field", $checkedField, true); // unique

											} else {
                                             //echo $post_id."=>".$checkedField;

											  add_post_meta($post_id, "checked_field", $checkedField, true); // unique
											}
               }
			   if (isset($_POST['icon'])) {
				$checkedField = array_map('sanitize_text_field', explode(',', $_POST['icon']));
				
				// Validate or perform additional checks if needed
				
				// Delete existing meta and update with new values
				delete_post_meta($post_id, 'icon');
				update_post_meta($post_id, 'icon', $checkedField);
			}
               if($_POST['meta_form_id']){
											$key_meta_form_id = get_post_meta( $post_id, "meta_form_id" );
											if( $key_meta_form_id[0]) {
													$current_custom_postmeta = get_post_meta($post_id, "meta_form_id", true);
                                                    if($current_custom_postmeta)
													{
														delete_post_meta($post_id, "meta_form_id");
													}
												 add_post_meta($post_id, "meta_form_id", $_POST['meta_form_id'], true); // unique

											} else {
                                             //echo $post_id."=>".$checkedField;

											  add_post_meta($post_id, "meta_form_id", $_POST['meta_form_id'], true); // unique
											}
               }

			  foreach ($_POST as $name => $value) {
		/* 		  echo $name."  ".$value."<br />";
				  if( str_contains($name, 'shortcode_')){
					  $key_1_values1 = get_post_meta( $post_id, $name);
					
						if( $key_1_values1[0]) {
									 $current_custom_postmeta = get_post_meta($post_id, $name, true);
													if($current_custom_postmeta)
													{  
												      delete_post_meta($post_id, $name);
													}
												 add_post_meta($post_id, $name, $value, true); // unique

											} else {
											  add_post_meta($post_id, $name, $value, true); // unique
											}
				  } */
					//foreach($_POST['checked_field'] as $checkedInput){
					//	if($checkedInput==$value){
							  if (str_contains($name, 'meta_') ) {
								//echo $name."<br />";
											$myvals = get_post_meta($post_id);
											$key_1_values = get_post_meta( $post_id, $name );
											if( $key_1_values[0]) {
													$current_custom_postmeta = get_post_meta($post_id, $name, true);
													if($current_custom_postmeta)
													{
														delete_post_meta($post_id, $name);
													}
												 add_post_meta($post_id, $name, $value, true); // unique

											} else {
											  add_post_meta($post_id, $name, $value, true); // unique
											}
									 

								}
					//}
				 // }
				
			} 
    }

    //if you get here then it's your post type so do your thing....
}




function download_and_save_file($url)
{

	$token = get_option("kobo_api_key");

	$auth_headers = array(
		'Authorization' => 'Token ' . $token,
	);

	$args = array(
		'headers' => $auth_headers,
	);

	// Determine the destination folder
	$upload_dir = wp_upload_dir();
	$download_folder = $upload_dir['basedir'] . '/kobo_downloads/';

	if (!file_exists($download_folder)) {
		mkdir($download_folder, 0755, true);
	}

	// Get the file name from the URL
	$url_path = parse_url($url, PHP_URL_PATH);
	$file_name = basename($url_path);



	// Check if the file already exists, and if not, download and save it
	if (!file_exists($download_folder . $file_name)) {
		$file_content = wp_remote_get($url, $args);

		if (is_array($file_content) && !is_wp_error($file_content)) {
			$body = $file_content['body'];
			$file_path = $download_folder . $file_name;
			file_put_contents($file_path, $body);

			// Optionally, you can do further processing with the downloaded file
			return $file_path;
		}
	}

	return false;
}

function download_and_save_file_with_curl($url)
{

	global $download_folder;

	$token = get_option("kobo_api_key");

	$auth_headers = array(
		'Authorization' => 'Token ' . $token,
	);

	if (!file_exists($download_folder)) {
		mkdir($download_folder, 0755, true);
	}

	// $url_path = parse_url($url, PHP_URL_PATH);
	// $file_name = basename($url_path);

	$file_name = extractFileName($url);

	//echo "************" . $file_name . "***************";

	$file_path = $download_folder . $file_name;

	// Check if the file already exists, and if not, download and save it
	if (!file_exists($file_path)) {
		$ch = curl_init($url);

		$fp = fopen($file_path, 'w');

		curl_setopt_array($ch, array(
			CURLOPT_URL => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'GET',
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_FILE => $fp,
			CURLOPT_HTTPHEADER => array(
				'Authorization: Token ' . $token,
				'Cookie: django_language=en'
			),
		));


		$result = curl_exec($ch);

		curl_close($ch);
		fclose($fp);

		if ($result) {
			return $file_path;
		}
	}

	return false; // File not downloaded
}


function extractFileName($url)
{
	//$url = 'https://kc.kobotoolbox.org/media/original?media_file=santiagolalibre%2Fattachments%2Fe39921fb4f68459cb5d5dfca32fd928d%2F76c82b1c-e1d7-48cf-9fc4-2e62b24f2645%2FImagen_de_WhatsApp_2023-09-18_a_las_16.54.36111-1_26_21.jpg';

	// Split the URL by "%2F"
	$urlParts = explode("%2F", $url);

	$imageName = end($urlParts);
	$imageName = urldecode($imageName);

	return $imageName;
}





function add_custom_submenu_page()
{

	$page_title = 'Kobo Settings';
	$menu_title = 'Kobo Settings';
	$capability = 'edit_posts';
	$menu_slug = 'kobo_submenu';
	$function = 'kobo_submenu_page_content';

	// Hook the submenu page
	add_submenu_page(
		'edit.php?post_type=kobo_maps',
		$page_title,
		$menu_title,
		$capability,
		$menu_slug,
		$function
	);
}
add_action('admin_menu', 'add_custom_submenu_page');

function kobo_submenu_page_content()
{
    if (isset($_POST["kobo_settings"])) {
        $get_url = esc_attr($_POST["get_url"]);
        $user_name = esc_attr($_POST["user_name"]);
        $pass = esc_attr($_POST["password"]);

        update_option("get_url", $get_url);
        update_option("user_name", $user_name);
        update_option("password", $pass);
    }

    $get_url = get_option("get_url");
    $uname = get_option("user_name");
    $pass = get_option("password");

    echo "<style>
    
    .kobo-wrap{
        background-color: #fff;
        max-width: 600px;
        padding: 10px 20px 30px 20px;
        border-radius: 10px;
        margin-top: 20px;
    }

    .kobo-wrap h3{
        padding:0;
        margin:0;
    }

    .kobo-wrap input{
        width: 100%;
    }

    .kobo-wrap input[type='submit']{
        width: auto;
        background-color: #333;
        border: 1px solid #333;
        margin-top: 10px;
        border-radius: 6px;
        padding: 6px 40px;
        color: #fff;
    }

    </style>";

    echo '<div class="kobo-wrap"><h2>Kobo Settings</h2>';
    echo '<p>Api User Name and Password</p>';
    echo "<form method='POST'>";
    echo "<input type='hidden' name='kobo_settings' value='1'>";
    echo "<input type='text' placeholder='api url' name='get_url' value='{$get_url}'>";
    echo "<input type='text' placeholder='username'  name='user_name' value='{$uname}'>";
    echo "<input type='password' placeholder='password'   name='password' value='{$pass}'>";
    echo "<input type='submit' name='submit' value='Save'>";
    echo "</form>";

    // Display the fetched form data
    if (!is_wp_error($form_data)) {
        echo '<h3>Form Data:</h3>';
        echo '<pre>';
        // print_r($form_data);
        echo '</pre>';
    }

    // Display the fetched API data
    if (!is_wp_error($api_data)) {
        echo '<h3>API Data:</h3>';
        echo '<pre>';
        print_r($api_data);
        echo '</pre>';
    }

    echo '</div>';
}

// Function to fetch form data from KoboToolbox
function get_kobo_form_data($username,$password)
{
    $koboUrl = 'https://kc.kobotoolbox.org';
    $apiEndpoint = $koboUrl . '/api/v1/forms';

    $response = wp_remote_get($apiEndpoint, array(
        'headers' => array(
            'Authorization' => 'Basic ' . base64_encode( 'username:password' )
        ),
    ));

    if (is_wp_error($response)) {
        return $response;
    }

    $body = wp_remote_retrieve_body($response);
    $form_data = json_decode($body, true);
    echo  $form_data ;die;
    if (empty($form_data) || !isset($form_data['results'])) {
        return new WP_Error('no_data', 'No form data found.');
    }

    return $form_data['results'];
}

// Function to fetch API data from KoboToolbox
function get_kobo_api_data($apiKey)
{
    $koboUrl = 'http://kc.kobotoolbox.org/';
    $apiEndpoint = $koboUrl . 'api/v2/assets';

    $response = wp_remote_get($apiEndpoint, array(
        'headers' => array(
            'Authorization' => 'Token ' . $apiKey,
        ),
    ));

    if (is_wp_error($response)) {
        return $response;
    }

    $body = wp_remote_retrieve_body($response);
    $api_data = json_decode($body, true);

    if (empty($api_data) || !isset($api_data['results'])) {
        return new WP_Error('no_data', 'No API data found.');
    }

    return $api_data['results'];
}


