<?php


function downloadFileWithAuthHeader($url, $token, $destination) {

    
    // Initialize cURL session
    $ch = curl_init($url);

    // Set cURL options
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Token ' . $token, // Add the authorization header
    ));
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true); // Follow redirects
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); // Disable SSL host verification (use for testing only)
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Disable SSL peer verification (use for testing only)

    // Execute cURL session and save the result to the destination file
    $result = curl_exec($ch);

    // Check for errors
    if (curl_errno($ch)) {
        return 'Error: ' . curl_error($ch);
    }

    // Save the downloaded content to the destination file
    file_put_contents($destination, $result);

    // Close cURL session
    curl_close($ch);

    return true;
}