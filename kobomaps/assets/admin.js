jQuery.noConflict();
(function( $ ) {
  $(function() {
    
    $("#kobo-select-fields").select2({
        width: "100%"
    });

    $("#kobo-long-field").select2({
        width: "100%"
    });

    $("#kobo-lat-field").select2({
      width: "100%"
  }); 


  });
})(jQuery);