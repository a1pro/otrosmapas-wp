
var koboHeader = kobo_map_data.header;
var koboBody =  kobo_map_data.body;
var koboIncluded = kobo_map_data.include;

var koboLong = kobo_map_data.long;
var koboLat = kobo_map_data.lat;

var centerLong = kobo_map_data.center_long;
var centerLat = kobo_map_data.center_lat;

var fileUploadPath = kobo_map_data.file_upload_path;

// console.log(centerLong);

//var koboIncluded = [0, 1, 2, 3, 10, 15, 23];
var koboCoordinates = [koboLat, koboLong]; //keys of the coordinates on map
// console.log("koboBody:::", koboBody);
// koboBody = koboBody[0];

var map = L.map('kobo-map').setView([centerLat, centerLong], 10);
// Add a tile layer to the map
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
}).addTo(map);

var resultMaps = {};

//120, 121


for(i = 0; i < koboBody[0].length; i++){
    resultMaps[i] = [];
    // console.log(koboBody[0].length);

    resultMaps[i].push({"lat" : koboBody[koboCoordinates[0]][i]});
    resultMaps[i].push({"long" : koboBody[koboCoordinates[1]][i]});

    var singlePopupStr = "";
    
    for(j = 0; j < koboIncluded.length; j++){
        let key = koboIncluded[j];
        let hh = koboHeader[key];
        resultMaps[i].push({ [hh]: koboBody[key][i]});
        let isUrl = koboIsUrl(koboBody[key][i]);
        
        if(isUrl){
            let embedCode = "";
            if(checkIfAudio(koboBody[key][i])){
                embedCode = koboMakeVideoCode(koboBody[key][i]);
            }else if(checkIfVideo(koboBody[key][i])){
                embedCode = koboMakeVideoCode(koboBody[key][i]);
                // alert("video");
            }else if(checkIfImage(koboBody[key][i])){
                embedCode = koboMakeImageCode(koboBody[key][i]);
            }else{
                embedCode = koboMakeUrlCode(koboBody[key][i]);
            }

            singlePopupStr += `<p><b>${hh}: </b><span>${embedCode}</span></p>`;
        }else{
            singlePopupStr += `<p><b>${hh}: </b><span>${koboBody[key][i]}</span></p>`;
        }

        
        // resultMaps[i][hh] = koboBody[key][i];
    }

    let lat = parseFloat(resultMaps[i][0].lat);
    let long = parseFloat(resultMaps[i][1].long);
    //console.log(lat);
    //console.log(long);

    if(!isNaN(lat) && !isNaN(long)){
        var marker = L.marker([lat, long]).addTo(map);
        var customPopup = `<div class='kobo-popup'>${singlePopupStr}</div>`;
        marker.bindPopup(customPopup);
    }

}//ef



// for(i = 0; i < koboIncluded.length; i++){
//     let key = koboIncluded[i];
//     let firstTime = true;
//     for(j = 0; j < koboBody[key].length; j++){
//         // console.log(koboBody[key][j]);
//         let hh = koboHeader[j];
//         resultMaps[i].push({ hh: koboBody[key][j]});
//     }//ef
// }//ef


console.log(resultMaps);



// function checkURLTypeByUrl(url) {
//     const extension = url.split('.').pop().toLowerCase();
  
//     if (extension.match(/\.(jpg|jpeg|png|gif|bmp|webp)$/i)) {
//       return 'image';
//     } else if (extension.match(/\.(mp4|webm|ogv|avi)$/i)) {
//       return 'video';
//     } else {
//       return 'other';
//     }
// }

function checkIfImage(url) {
   return /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(url);
}


function checkIfVideo(url) {
    return /\.(mp4|avi)$/.test(url);
}

function checkIfAudio(url) {
    return /\.(mp3|wav|opus)$/.test(url);
}


function koboIsUrl(str) {
    const pattern = /^(https?|ftp):\/\/[^\s/$.?#].[^\s]*$/i;
    return pattern.test(str);
  }
  

function koboMakeVideoCode(url){
    let fileName = extractImageName(url);
    let fileUrl = fileUploadPath + fileName;
    return `<video width='640' height='360' controls><source src='${fileUrl}' type='video/mp4'></video>`;
}

function koboMakeAudioCode(url){
    let fileName = extractImageName(url);
    let fileUrl = fileUploadPath + fileName;
    return `<audio controls><source src='${fileUrl}' type='audio/ogg'><source src='${fileUrl}' type='audio/mpeg'></audio>`;
}

function koboMakeImageCode(url){
    //fileUploadPath
    let fileName = extractImageName(url);
    let fileUrl = fileUploadPath + fileName;
    return `<img src='${fileUrl}'>`;
}

function koboMakeUrlCode(url){
    return `<a target='_blank' href='${url}'>Open Url</a>`;
}



function extractImageName(url){
    //var url = 'https://kc.kobotoolbox.org/media/original?media_file=santiagolalibre%2Fattachments%2Fe39921fb4f68459cb5d5dfca32fd928d%2F76c82b1c-e1d7-48cf-9fc4-2e62b24f2645%2FImagen_de_WhatsApp_2023-09-18_a_las_16.54.36111-1_26_21.jpg';

    // Split the URL by "%2F" and get the last part
    var urlParts = url.split('%2F');
    var imageName = decodeURIComponent(urlParts[urlParts.length - 1]);

    // console.log(imageName);
    return imageName;
}